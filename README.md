# This is a clone of the popular game of bricks -- BreakOut --!
**Made with Python 3.X and Pygame [http://pygame.org/]**

*This code is rolling release, and only for educational purporses*

### GOALS
* Make an _Start Game_ first escene
* Several levels, increasing their dificulty and change each stage.
* Better design and animations
* If the player wins the game, make a final design of the history and disply credits.

